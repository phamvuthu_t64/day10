<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="page.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    
    <title>Document</title>
</head>
<?php 
     if ($_SERVER['REQUEST_METHOD'] == "POST") // Gửi yêu cầu cho sever biết khi nhấn nút đăng kí
     {
        $_COOKIE = $_POST;
        
        $q1=  $_COOKIE['q&a1'];
        $q2=  $_COOKIE['q&a2'];
        $q3=  $_COOKIE['q&a3'];
        $q4=  $_COOKIE['q&a4'];
        $q5=  $_COOKIE['q&a5'];
     
     
        if (isset($_POST['Next'])) 
          {  
                setcookie( 'q&a1', $q1);
                setcookie( 'q&a2', $q2);
                setcookie( 'q&a3', $q3);
                setcookie( 'q&a4', $q4);
                setcookie( 'q&a5', $q5);

                header('location: page2.php');

          }

                    }

    ?>
<body>
<form action='' method='POST'>
    <div class="containerr  my-1">
        <H1 class="text-center">Trang 1</H1>
        
        
        
        <br>
        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 h5"><b>1. Nhóm nhạc nào sau đây thuộc công ty SM ENTERTAINMENT ?</b></div>
            
        <?php
          

         
        
        
        $question = array( "A" => "SNSD", "B" => "BIGBANG","C" => "T-ARA","D" => "2NE1");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </Br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='q&a1' value='".$value. "''
            
            ";
            
            echo isset($_COOKIE['q&a1']) && $_COOKIE['q&a1'] == $value ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>2 .Đâu là nhóm nhạc Gen 4 của SM ?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "SNSD", "B" => "aespa","C" => "F(x)","D" => "RedVelvet");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='q&a2' value='".$value. "''
            
            ";
            
            echo isset($_COOKIE['q&a2']) && $_COOKIE['q&a2'] == $value ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>3 .Đâu là bài hát đầu tiên làm lên tên tuổi của SNSD?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Lion Heart", "B" => "Mr.Taxi","C" => "Gee","D" => "Oh!");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='q&a3' value='".$value. "''
            
            ";
            
            echo isset($_COOKIE['q&a3']) && $_COOKIE['q&a3'] == $value ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>4 .Leader của EXO là ?</b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Sehun", "B" => "Baekhyun","C" => "Suho","D" => "Xuimin");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='q&a4'  value='".$value. "''
            ";
            
            echo isset($_COOKIE['q&a4']) && $_COOKIE['q&a4'] == $value ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 

        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 mt-5 h5"><b>5 . Đâu là bài hát debut của Redvelvet ? </b></div>
            
        <?php
        
        
        
        $question = array( "A" => "Red Flavor", "B" => "Russia Roulette","C" => "Badboy","D" => "Happiness");
        foreach ($question as $i => $value)
              {
            echo
            "<br> </br>
            <label class='options'>
            <input class='answer' type='radio'  class='question' name='q&a5'  value='".$value. "''
            
            ";
            
            echo isset($_COOKIE['q&a5']) && $_COOKIE['q&a5'] == $value ? " checked" : "";
            echo "/>
            <span class='checkmark'></span>
                </label> ".$value;
            }
         ?>
         
            
        </div> 
        
                
        
         
                
        
        <div class="d-flex align-items-center pt-3">

            <div class="ml-auto mr-sm-5  Next1">
                
                <input class="btn btn-success" name='Next' type="submit" value="Next"></input>
            </div>
        </div>
        
    </div>
    
</body>
</html>
