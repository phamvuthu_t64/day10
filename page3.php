<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="page.css">
  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

  <title>Document</title>
</head>

<body>
  <form action='' method='POST'>
    <div class="containerr  my-1">
      <H1 class="text-center">Kết quả</H1>

      <?php
      $totalscore = 0;

      $question = 0;
      $zero = "Bạn chưa chọn đáp án!";
      $keys = array("q&a1" => "SNSD",
                    "q&a2" => "aespa",
                    "q&a3" => "Gee",
                    "q&a4" => "Suho",
                    "q&a5" => "Happiness",
                    "q&a6" => "01/01/2001",
                    "q&a7" => "Hậu duệ mặt trời",
                    "q&a8" => "7",
                    "q&a9" => "NCT 127",
                    "q&a10" => "15" );
      $question1 = array(  "" => "",
                          "1" => "1. Nhóm nhạc nào sau đây thuộc công ty SM ENTERTAINMENT ?",
                          "2" => "2 .Đâu là nhóm nhạc Gen 4 của SM ?",
                          "3" => "3 .Đâu là bài hát đầu tiên làm lên tên tuổi của SNSD?",
                          "4" => "4 .Leader của EXO là ?",
                          "5" => "5 . Đâu là bài hát debut của Redvelvet ? ",
                          "6" => "6 .Sinh nhật của Winner aespa ?",
                          "7" => "7 .Onew Shinee đã từng tham gia bộ phim nào ?",
                          "8" => "8 .NCT Dream gồm mấy thành viên?",
                          "9" => "9 .Kick it là bài hát của nhóm nhạc nào?",
                          "10" => "10 .Super Junior đã thành lập được bao nhiêu năm?" );

      foreach (array_keys($keys) as $k) {
        ++$question;
        foreach ($question1 as $i => $value) {

          if ($question == $i) {
            if (!empty($_COOKIE[$k])) {
              if ($keys[$k] == $_COOKIE[$k]) {
                ++$totalscore;
                echo
                "<br> </br>
                 <div class='question ml-sm-5 pl-sm-5 pt-2 '>
                 <div class='py-2 mb-5  h5'><b>";
                echo "" . $value;
                echo "</b></div>
                                          
                  </div> 
                  <div style='display: flex ;'>
                  <label class='options'>
                  <input class='answer' type='radio'  class='question' checked='true'    value=''
                  
                  ";


                echo "  />
                                           
                  <span class='checkmark1'>
                  </span>
                  
                  
                      </label> <div style='color: #21bf73 ;'>" . $_COOKIE[$k];
                echo "</div>
                 </div>";
              }
               else if ($keys[$k] != $_COOKIE[$k]) 
               {
                echo
                "<br> </Br>
                <div class='question ml-sm-5 pl-sm-5'>
                <div class='py-2 mb-5 h5'><b>";echo"".$value; echo "</b></div>
                 
                  </div> 
                  <div style='display: flex ;'>
                      <label class='options'>
                      <input class='answer'  type='radio'  class='question'  checked='true'    value=''
                      ";
 
                      
                echo " />
                <span class='checkmark1'></span>
                    </label> <div style='color: #21bf73 ;'> " . $keys[$k];
                echo
                "
                </div>
                </div>
                <br> </Br>
                <div style='display: flex ;'>
                <label class='options'>
                <input class='answer'  type='radio'  class='question'  checked='true'    value=''
                
                ";


                echo "  />
                <span class='checkmark2'></span>
                    </label> <div style='color: red ;'>" . $_COOKIE[$k];
                echo
                "
                </div>
                </div>
                ";
              }
            }



            if (empty($_COOKIE[$k])) {
              echo
              "<br> </Br>
                <div class='question ml-sm-5 pl-sm-5'>
            <div class='py-2 mb-5 h5'><b>";
              echo "" . $value;
              echo "</b></div>
                                      
                </div> 
                <div style='display: flex ;'>
                    <label class='options'>
                    <input class='answer'  type='radio'  class='question'  checked='true'    value=''
                    
                    ";


              echo " />
              <span class='checkmark1'></span>
                  </label> <div style='color: #21bf73 ;'> " . $keys[$k];
              echo
              "
              </div>
              </div>
              <br> </Br>
              <div style='display: flex ;'>
              <label class='options'>
              <input class='answer'  type='radio'  class='question'  checked='true'    value=''
              
              ";


              echo "  />
                <span class='checkmark2'></span>
                    </label> <div style='color: red ;'>" . $zero;
              echo
              "
              </div>
              </div>
              ";
            }
          }
        }
      }

      echo "<h3 id='results' class='question pt-3'>Bạn đã trả lời đúng $totalscore /10</h3>";
      if ($totalscore <= 4) {
        echo "Bạn quá kém,cần ôn tập thêm";
      }

      if ($totalscore > 4 && $totalscore < 7) {
        echo "Cũng bình thường";
      }

      if ($totalscore >= 7) {
        echo "Sắp sửa làm được trợ giảng lớp PHP";
      }

      ?>

    </div>
    </div>
</body>

</html>